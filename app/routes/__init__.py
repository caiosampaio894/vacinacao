from flask import Flask
from .user_vaccination_blueprint import bp as bp_vaccin

def init_app(app: Flask):
    app.register_blueprint(bp_vaccin)