from flask import Blueprint
from app.controllers.user_vaccination_controller import  create_user_vaccination, get_user_vaccination



bp = Blueprint('user_vaccination', __name__, url_prefix='/vaccination')

bp.post('')(create_user_vaccination)
bp.get('')(get_user_vaccination)
