from sqlalchemy.sql.functions import user
from app.models.user_vaccination_model import UserVaccinationModel
from flask import jsonify, request, current_app
import sqlalchemy
from sqlalchemy import exc
import psycopg2
from app.models.user_vaccination_model import UserVaccinationModel
from app.exc.errors import InvalidCpf

def create_user_vaccination():
    data = request.json
    try:
        user_vaccination = UserVaccinationModel(**data)
        UserVaccinationModel.treatCpf(data)
        session = current_app.db.session
        session.add(user_vaccination)
        session.commit()

        return jsonify(user_vaccination), 201

    except InvalidCpf as e:
        return {'message': str(e)}, 404
    

        

def get_user_vaccination():
    user_vaccination_list = UserVaccinationModel.query.all()
    return jsonify(user_vaccination_list), 200