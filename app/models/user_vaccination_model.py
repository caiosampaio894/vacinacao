from app.configs.database import db
from dataclasses import dataclass
from datetime import datetime, timedelta
from sqlalchemy.sql.sqltypes import DateTime, Numeric
from sqlalchemy import Column, String
from app.configs.database import db
from app.exc.errors import InvalidCpf

@dataclass
class UserVaccinationModel(db.Model):
    cpf: str
    name: str
    vaccine_name: str
    first_shot_date: datetime
    second_shot_date: datetime
    health_unit_name: str

    __tablename__ = 'vaccine_card'

    cpf = Column(String, primary_key=True)
    name = Column(String, nullable=False)
    first_shot_date = Column(DateTime, default=datetime.utcnow)
    second_shot_date = Column(DateTime, default=(datetime.utcnow() + timedelta(90)))
    vaccine_name = Column(String, nullable=False)
    health_unit_name = Column(String)
    
    @staticmethod
    def treatCpf(data):
        if len(data['cpf']) != 11 or data['cpf'].isnumeric() == False:
            raise InvalidCpf('O cpf deve ser numérico e conter 11 dígitos.')

