from flask_migrate import Migrate
from flask import Flask


def init_app(app: Flask):

    from app.models.user_vaccination_model import UserVaccinationModel

    Migrate(app, app.db)
